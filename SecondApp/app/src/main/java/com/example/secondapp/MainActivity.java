package com.example.secondapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import com.example.secondapp.receivers.*;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GeneralReceiver gr = new GeneralReceiver();
        registerReceiver(gr,new IntentFilter("com.example.securazediagnose"));


//        BLUETOOTH ----------------------------------------------------
        Intent bluetoothIntent = new Intent();
        bluetoothIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.connectivity.Bluetooth")); // (other app package name, other app class)
        startService(bluetoothIntent);

//        SIM AND SIGNAL --------------------------------------------------
        Intent simIntent = new Intent();
        simIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.connectivity.SimAndSignal")); // (other app package name, other app class)
        startService(simIntent);

//        WIFI ------------------------------------------------------------
        Intent wifiIntent = new Intent();
        wifiIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.connectivity.Wifi")); // (other app package name, other app class)
        startService(wifiIntent);

//        HOME AND RECENTS BUTTON ------------------------------------------------------------ NOT COMPLETE!!!
//        Intent i = new Intent();
//        i.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.buttons.homeAndRecents.HomeAndRecentsButtonService")); // (other app package name, other app class)
//        startService(i);

//        ACCELEROMETER ------------------------------------------------------------
        Intent accelerometerIntent = new Intent();
        accelerometerIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Accelerometer")); // (other app package name, other app class)
        startService(accelerometerIntent);

//        BAROMETER ------------------------------------------------------------
        Intent barometerIntent = new Intent();
        barometerIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Barometer")); // (other app package name, other app class)
        startService(barometerIntent);


//        COMPASS ------------------------------------------------------------
        Intent compassIntent = new Intent();
        compassIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Compass")); // (other app package name, other app class)
        startService(compassIntent);

//        FINGERPRINT ------------------------------------------------------------
        Intent fingerPrintIntent = new Intent();
        fingerPrintIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Fingerprint")); // (other app package name, other app class)
        startService(fingerPrintIntent);

//        GRAVITY ------------------------------------------------------------
        Intent gravityIntent = new Intent();
        gravityIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Gravity")); // (other app package name, other app class)
        startService(gravityIntent);

//        GYROSCOPE ------------------------------------------------------------
        Intent gyroscopeIntent = new Intent();
        gyroscopeIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Gyroscope")); // (other app package name, other app class)
        startService(gyroscopeIntent);

//        PROXIMITY ------------------------------------------------------------
        Intent proximityIntent = new Intent();
        proximityIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Proximity")); // (other app package name, other app class)
        startService(proximityIntent);

//        SIGNIFICANT MOTION ------------------------------------------------------------
//        Intent i = new Intent();
//        i.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.SignificantMotion")); // (other app package name, other app class)
//        startService(i);

//        TEMPERATURE ------------------------------------------------------------
        Intent temperatureIntent = new Intent();
        temperatureIntent.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.sensors.Temperature")); // (other app package name, other app class)
        startService(temperatureIntent);

//        GPS ------------------------------------------------------------
//        Intent i = new Intent();
//        i.setComponent(new ComponentName("com.example.securazediagnose", "com.example.securazediagnose.services.GPS")); // (other app package name, other app class)
//        startService(i);
    }
}
