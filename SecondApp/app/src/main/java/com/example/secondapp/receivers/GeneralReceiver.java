package com.example.secondapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.widget.Toast;


// We could probably use an universal receiver for everything, but in that case the code would be much less readable. So my recommendation is that every case has its own receiver.
public class GeneralReceiver extends BroadcastReceiver {
    boolean caseResult = false;
    float x = 0;
    float y = 0;
    float z = 0;
    String message = "";
    double longitude;
    double latitude;
    @Override
    public void onReceive(Context context, Intent intent) {
        System.out.println("Action is: " + intent.getAction());
        switch (intent.getAction()){
            case "BLUETOOTH_CASE":
                caseResult = intent.getBooleanExtra("BluetoothCase",false); //First argument is the key, second argument is the default value (if no value is received)
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult , Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                break;
            case "SIM_CASE":
                caseResult = intent.getBooleanExtra("SimCase",false); //First argument is the key, second argument is the default value (if no value is received)
                int signalStrength = intent.getIntExtra("SignalStrength", 0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult + "\nSignal strength: " + signalStrength, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                break;
            case "WIFI_CASE":
                caseResult = intent.getBooleanExtra("WifiCase",false); //First argument is the key, second argument is the default value (if no value is received)
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                break;
            case "SOUND_AND_MIC_CASE":
                caseResult = intent.getBooleanExtra("AudioRecorderCase",false); //First argument is the key, second argument is the default value (if no value is received)
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                break;
            case "SOUND_CASE":
                caseResult = intent.getBooleanExtra("SoundCase",false); //First argument is the key, second argument is the default value (if no value is received)
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                break;
            case "ACCELEROMETER_CASE":
                caseResult = intent.getBooleanExtra("AccelerometerCase",false); //First argument is the key, second argument is the default value (if no value is received)
                x = intent.getFloatExtra("AccelerometerX",0);
                y = intent.getFloatExtra("AccelerometerY",0);
                z = intent.getFloatExtra("AccelerometerZ",0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("X: " + x);
                System.out.println("Y: " + y);
                System.out.println("Z: " + z);
                break;
            case "BAROMETER_CASE":
                caseResult = intent.getBooleanExtra("BarometerCase",false); //First argument is the key, second argument is the default value (if no value is received)
                float pascals = intent.getFloatExtra("Pascals",0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult + "\nPascals: " + pascals, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("Pascals: " + pascals);
                break;
            case "COMPASS_CASE":
                caseResult = intent.getBooleanExtra("CompassCase",false); //First argument is the key, second argument is the default value (if no value is received)
                x = intent.getFloatExtra("CompassX",0);
                y = intent.getFloatExtra("CompassY",0);
                z = intent.getFloatExtra("CompassZ",0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("X: " + x);
                System.out.println("Y: " + y);
                System.out.println("Z: " + z);
                break;
            case "FINGERPRINT_CASE":
                caseResult = intent.getBooleanExtra("FingerprintCase",false); //First argument is the key, second argument is the default value (if no value is received)
                message = intent.getStringExtra("Message");
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult + "\nMessage: " + message, Toast.LENGTH_LONG).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("Message: " + message);
                break;
            case "GRAVITY_CASE":
                caseResult = intent.getBooleanExtra("GravityCase",false); //First argument is the key, second argument is the default value (if no value is received)
                x = intent.getFloatExtra("GravityX",0);
                y = intent.getFloatExtra("GravityY",0);
                z = intent.getFloatExtra("GravityZ",0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("X: " + x);
                System.out.println("Y: " + y);
                System.out.println("Z: " + z);
                break;
            case "GYROSCOPE_CASE":
                caseResult = intent.getBooleanExtra("GyroscopeCase",false); //First argument is the key, second argument is the default value (if no value is received)
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                break;
            case "PROXIMITY_CASE":
                caseResult = intent.getBooleanExtra("ProximityCase",false); //First argument is the key, second argument is the default value (if no value is received)
                x = intent.getFloatExtra("ProximityX",0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("Proximity: " + x);
                break;
            case "SIGNIFICANT_MOTION_CASE": //NOT TESTED!!!
                caseResult = intent.getBooleanExtra("SignificantMotionCase",false); //First argument is the key, second argument is the default value (if no value is received)
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                break;
            case "TEMPERATURE_CASE":
                caseResult = intent.getBooleanExtra("TemperatureCase",false); //First argument is the key, second argument is the default value (if no value is received)
                x = intent.getFloatExtra("Temperature",0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult + "\nTemperature: " + x, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("Temperature: " + x);
                break;
            case "GPS_CASE":
                caseResult = intent.getBooleanExtra("GPSCase",false); //First argument is the key, second argument is the default value (if no value is received)
                longitude =  intent.getDoubleExtra("Longitude",0);
                latitude =  intent.getDoubleExtra("Latitude",0);
                Toast.makeText(context, "Received data from: " + intent.getAction() +"\nCase result: " + caseResult + "\nGPS Location: Longitude: " + longitude + " Latitude: " + latitude, Toast.LENGTH_SHORT).show();
                System.out.println("Received data from: " + intent.getAction());
                System.out.println("Longitude: " + longitude);
                System.out.println("Latitude: " + latitude);
                break;

        }

    }
}
