package com.example.securazediagnose;

public interface EventListener {

    public void sendDataToActivity(String data);

}