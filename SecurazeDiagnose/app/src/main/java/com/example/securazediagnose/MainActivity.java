package com.example.securazediagnose;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.FrameLayout;

import com.example.securazediagnose.adb.NfcCommand;
import com.example.securazediagnose.fragments.CheckboxView;
import com.example.securazediagnose.fragments.DetailedDescriptionView;
import com.example.securazediagnose.fragments.ProgressView;

public class MainActivity extends AppCompatActivity implements KeyEvent.Callback, EventListener {
    NfcCommand nfc;

    int currentPage = 1;
    ViewPager viewPager;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        int PERMISSION_ALL = 1;

        String[] PERMISSIONS = {
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.USE_FINGERPRINT,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.CAMERA
        };
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        sendDataToActivity("1");


//        ACTIVITIES -----------------------------------------------------
//        Intent cameraPhotoIntent = new Intent(MainActivity.this, CameraPhoto.class);
//        MainActivity.this.startActivity(cameraPhotoIntent);
//
//        Intent cameraVideoIntent = new Intent(MainActivity.this, CameraVideo.class);
//        MainActivity.this.startActivity(cameraVideoIntent);
//
//        Intent volumeControllIntent = new Intent(MainActivity.this, VolumeControll.class);
//        volumeControllIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        MainActivity.this.startActivity(volumeControllIntent);

//        SERVICES -----------------------------------------------------------

//              BUTTONS--------------------------
//        startHomeButtonService();

//              CONNECTIVITY---------------------
//        startBluetoothService();
//        startWifiService();
//        startSimService();

//              MEDIA----------------------------
//        startPlaySoundService();
//        startAudioRecorderService();

//              SENSORS--------------------------
//        startSignificantMotionService();
//        startGravityService();
//        startAccelometerService();
//        startGyroscopeService();
//        startProximityService();
//        startCompassService();
//        startTemperatureService();
//        startBarometerService();
//        startFingerprintService();

//        OTHER----------------------------------
//        startGPSService();



//        nfc = new NfcCommand();
//        try {
//            nfc.grantPermission();
////            System.out.println(nfc.checkPermissions());
//            nfc.activateNFC();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }


    }

    @Override
    public void sendDataToActivity(String data) {

        System.out.println("data: " + data);
        System.out.println("data: " + data.split(",")[0]);
        Fragment selectedFragment = null;
        switch (data.split(",")[0]){
            case "1":
                selectedFragment = new CheckboxView();
                break;
            case "2":
                selectedFragment = new ProgressView();
                break;
            case "3":
                selectedFragment = new DetailedDescriptionView();
                break;
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment ).commit();

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }




}
