package com.example.securazediagnose;

import android.hardware.SensorEvent;

public interface Test {
    public void testSuccess();
    public void testFailure();
}
