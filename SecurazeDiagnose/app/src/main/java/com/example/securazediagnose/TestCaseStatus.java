package com.example.securazediagnose;

public class TestCaseStatus {
    status currentStatus = status.IN_PROGRESS;
    String testName = "";

    enum status {
        IN_PROGRESS,
        PASSED,
        FAILED
    }

    public status getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(status currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }


}
