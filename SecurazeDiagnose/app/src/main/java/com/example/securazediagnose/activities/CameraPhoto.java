package com.example.securazediagnose.activities;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.securazediagnose.R;
import com.example.securazediagnose.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CameraPhoto extends Activity implements SurfaceHolder.Callback, Test {


    private CameraManager mCameraManager;
    private int mCameraId = -1;
    private String lastImagePath;
    private boolean isAutoFocused;

    public static final int CHECK_PHOTO = 1;
    public static final int CHECK_FLASH = 2;
    public static final int CHECK_FOCUS = 3;
    private int checkMode = CHECK_PHOTO;

    public static final int FRONT_CAMERA = 1;
    public static final int BACK_CAMERA = 2;
    private int cameraOrientation = BACK_CAMERA;

    private enum CAMERA {
        CHECK_PHOTO,
        CHECK_FLESH,
        CHECK_FOCUS
    };

    private enum CAMERA_ORIENTATION {
        FRONT_CAMERA,
        BACK_CAMERA
    }

    Intent intent;
    String imagePath = "";

    ImageView iv_image;
    SurfaceView sv;
    SurfaceHolder sHolder;
    Camera mCamera;
    Camera.Parameters parameters;
    Bitmap bmp;
    Button passButton;
    Button failButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_photo);
        lastImagePath = "";
        isAutoFocused = false;
        intent = new Intent("android.media.action.IMAGE_CAPTURE");

        passButton = findViewById(R.id.passButton2);
        failButton = findViewById(R.id.failButton2);

        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testSuccess();
            }
        });

        failButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testFailure();
            }
        });

        if(cameraOrientation == FRONT_CAMERA){
            mCameraId = getFrontCameraId(); //getFrontCameraId()
        }
        else{
            mCameraId = getBackCameraId();
        }
        if (mCameraId == -1){
            Toast.makeText(getApplicationContext(), "No camera found.", Toast.LENGTH_LONG).show();
        }
        else
        {
            iv_image = (ImageView) findViewById(R.id.imageView);
            if(cameraOrientation == FRONT_CAMERA){
                iv_image.setRotation(270);
            }else{
                iv_image.setRotation(90);
            }

            sv = (SurfaceView) findViewById(R.id.surfaceView);
            sHolder = sv.getHolder();
            sHolder.addCallback((SurfaceHolder.Callback) this);
            sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }

        if(checkMode == CHECK_FLASH){
            flashOn();
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            flashOff();
        }
    }
    public void setCameraOrientation(int orient){
        this.cameraOrientation = orient;
    }

    public void setCheckMode(int mode){
        this.checkMode = mode;
    }

    public boolean getIsAutoFocused(){
        return this.isAutoFocused;
    }

    public String getLastImagePath(){
        return this.lastImagePath;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        setCameraDisplayOrientation(this, mCameraId ,mCamera);
        parameters = mCamera.getParameters();
        if(checkMode == CHECK_FOCUS){
            boolean focusAvailable = false;
            PackageManager pm = getPackageManager();
            if(pm.hasSystemFeature(PackageManager.FEATURE_CAMERA_AUTOFOCUS)){
                focusAvailable = true;
            }
            if(focusAvailable){
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            }
        }
        mCamera.setParameters(parameters);
        mCamera.startPreview();


        final Camera.PictureCallback mCall = new Camera.PictureCallback()
        {
            @Override
            public void onPictureTaken(byte[] data, Camera camera)
            {
                Uri uriTarget = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
                lastImagePath = uriTarget.toString();
                OutputStream imageFileOS;
                try {
                    imageFileOS = getContentResolver().openOutputStream(uriTarget);
                    Log.d("MainActivity", "uriTarget : " + uriTarget);
                    returnImagePath(uriTarget.toString());
                    imageFileOS.write(data);
                    imageFileOS.flush();
                    imageFileOS.close();
                }
                catch (FileNotFoundException e) {
                    e.printStackTrace();
                }catch (IOException e) {
                    e.printStackTrace();
                }
                bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
                iv_image.setImageBitmap(bmp);
            }
        };
        if(checkMode == CHECK_PHOTO){
//            mCamera.setDisplayOrientation(180);
            mCamera.takePicture(null, null, mCall);
        }
        else if(checkMode == CHECK_FOCUS){
            mCamera.autoFocus(new Camera.AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                    isAutoFocused = true;
                    Toast.makeText(getApplicationContext(), "FOCUSED", Toast.LENGTH_LONG).show();

                }
            });
        }




    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees + 180) % 360;
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    private static File getOutputMediaFile(int type, String path){
        File mediaStorageDir = new File(path, "MyCameraVideo");
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");

        return mediaFile;
    }

    int getFrontCameraId() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0 ; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) return i;
        }
        return -1; // No front-facing camera found
    }

    int getBackCameraId() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0 ; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK) return i;
        }
        return -1; // No front-facing camera found
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        boolean focusAvailable = false;


        mCamera = Camera.open(mCameraId);
        Toast.makeText(getApplicationContext(), "With camera", Toast.LENGTH_LONG).show();
        mCamera = Camera.open(mCameraId);
        try {
            mCamera.setPreviewDisplay(holder);

        } catch (IOException exception) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }

    public void flashOff() {
        boolean isFlashAvailable = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
        if (!isFlashAvailable) {
            //TEST FAILED
            System.out.println("No flash available");
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                mCameraId = Integer.parseInt(mCameraManager.getCameraIdList()[0]);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                mCameraManager.setTorchMode(Integer.toString(mCameraId), false);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public void flashOn(){
        boolean isFlashAvailable = getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT);
        if (!isFlashAvailable) {
            //TEST FAILED
            System.out.println("No flash available");
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                mCameraId = Integer.parseInt(mCameraManager.getCameraIdList()[0]);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            try {
                mCameraManager.setTorchMode(Integer.toString(mCameraId), true);
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
    }

    public String returnImagePath(String path){
        imagePath = path;
        return imagePath;
    }

    @Override
    public void testSuccess() {
        Intent i = new Intent();
        i.setClassName("com.example.secondapp", "com.example.secondapp.recievers.CameraPhoto"); // (other app package name, other app reciever class)
        if(checkMode == CHECK_FLASH){
            i.putExtra("CameraFlash", true); // The information that we want to send is sent by putExtra(key, value)
        }else if(checkMode == CHECK_FOCUS){
            i.putExtra("CameraFocus", true); // The information that we want to send is sent by putExtra(key, value)
        }else{
            i.putExtra("CameraPhoto", true); // The information that we want to send is sent by putExtra(key, value)
        }
        sendBroadcast(i);
        System.out.println("Test success.");
    }

    @Override
    public void testFailure() {
        Intent i = new Intent();
        i.setClassName("com.example.secondapp", "com.example.secondapp.recievers.CameraPhoto"); // (other app package name, other app reciever class)
        if(checkMode == CHECK_FLASH){
            i.putExtra("CameraFlash", false); // The information that we want to send is sent by putExtra(key, value)
        }else if(checkMode == CHECK_FOCUS){
            i.putExtra("CameraFocus", false); // The information that we want to send is sent by putExtra(key, value)
        }else{
            i.putExtra("CameraPhoto", false); // The information that we want to send is sent by putExtra(key, value)
        }
        sendBroadcast(i);
        System.out.println("Test success.");
    }
}
