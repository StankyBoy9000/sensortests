package com.example.securazediagnose.activities;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.example.securazediagnose.R;
import com.example.securazediagnose.Test;

import java.io.File;
import java.io.IOException;


public class CameraVideo extends Activity implements SurfaceHolder.Callback, Test {

    private int mCameraId = -1;
    private String lastVideoPath;

    public static final int FRONT_CAMERA = 1;
    public static final int BACK_CAMERA = 2;
    private int cameraOrientation = FRONT_CAMERA;

    Intent intent;
    String imagePath = "";
    VideoView videoView;
    SurfaceView sv;
    SurfaceHolder sHolder;
    Camera mCamera;
    Camera.Parameters parameters;
    MediaRecorder recorder;
    ToggleButton toggleButton;
    Button passButton;
    Button failButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_video);
        System.out.println("Video started");
        intent = new Intent("android.media.action.IMAGE_CAPTURE");
        if(cameraOrientation == FRONT_CAMERA){
            mCameraId = getFrontCameraId();
        }
        else{
            mCameraId = getBackCameraId();
        }
        if (mCameraId == -1){
            Toast.makeText(getApplicationContext(), "No front camera", Toast.LENGTH_LONG).show();
        }
        else
        {
            sv = (SurfaceView) findViewById(R.id.surfaceView2);
            sHolder = sv.getHolder();
            sHolder.addCallback((SurfaceHolder.Callback) this);
            sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        }
        passButton = findViewById(R.id.passButton);
        failButton = findViewById(R.id.failButton);

        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testSuccess();
            }
        });

        failButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testFailure();
            }
        });
        toggleButton = (ToggleButton) findViewById(R.id.toggleButton);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    try {
                        takeVideo();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    recorder.stop();  // stop the recording
                    if (recorder != null) {
                        recorder.reset();   // clear recorder configuration
                        recorder.release(); // release the recorder object
                        recorder = null;
                        mCamera.lock();           // lock camera for later use
                    }
                    showVideo();
                }
            }
        });



    }
    public void setCameraOrientation(int orient){
        this.cameraOrientation = orient;
    }


    public String getLastVideoPath(){
        return this.lastVideoPath;
    }


    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        parameters = mCamera.getParameters();
        mCamera.setParameters(parameters);
        setCameraDisplayOrientation(this, mCameraId ,mCamera);
        mCamera.startPreview();

    }

    public void showVideo(){
        VideoView videoView = (VideoView)findViewById(R.id.videoView);
        videoView.setVideoPath(lastVideoPath);
        videoView.start();
    }

    public void takeVideo() throws InterruptedException {
        prepareVideoRecorder();
        recorder.start();

    }

    public static void setCameraDisplayOrientation(Activity activity, int cameraId, android.hardware.Camera camera) {
        android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
        android.hardware.Camera.getCameraInfo(cameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0: degrees = 0; break;
            case Surface.ROTATION_90: degrees = 90; break;
            case Surface.ROTATION_180: degrees = 180; break;
            case Surface.ROTATION_270: degrees = 270; break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees + 180) % 360;
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    private boolean prepareVideoRecorder(){

        recorder = new MediaRecorder();
        mCamera.unlock();
        recorder.setCamera(mCamera);
        recorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        recorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
        recorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));
        if(cameraOrientation == FRONT_CAMERA){
            recorder.setOrientationHint(270);
        }else{
            recorder.setOrientationHint(90);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            recorder.setOutputFile(getOutputMediaFile());
        }else{
            recorder.setOutputFile(getOutputMediaFile().toString());
        }
        recorder.setPreviewDisplay(sv.getHolder().getSurface());
        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        return true;
    }


    private File getOutputMediaFile(){
        File new_folder = new File(getExternalFilesDir(null), "/VideoRecorder");
        if(!new_folder.exists()){
            new_folder.mkdir();
        }
        File mediaFile = new File(new_folder, "video.mp4");
        lastVideoPath = mediaFile.toString();
        return mediaFile;
    }

    int getFrontCameraId() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0 ; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) return i;
        }
        return -1; // No front-facing camera found
    }

    int getBackCameraId() {
        Camera.CameraInfo ci = new Camera.CameraInfo();
        for (int i = 0 ; i < Camera.getNumberOfCameras(); i++) {
            Camera.getCameraInfo(i, ci);
            if (ci.facing == Camera.CameraInfo.CAMERA_FACING_BACK) return i;
        }
        return -1; // No front-facing camera found
    }
    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        boolean focusAvailable = false;
        if (mCameraId == -1){
            Toast.makeText(getApplicationContext(), "No camera.", Toast.LENGTH_LONG).show();
        }
        else
        {
            mCamera = Camera.open(mCameraId);
            Toast.makeText(getApplicationContext(), "Camera found.", Toast.LENGTH_LONG).show();
        }
        mCamera = Camera.open(mCameraId);
        try {
            mCamera.setPreviewDisplay(holder);

        } catch (IOException exception) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        mCamera.stopPreview();
        mCamera.release();
        mCamera = null;
    }



    public String returnImagePath(String path){
        imagePath = path;
        return imagePath;
    }


    @Override
    public void testSuccess() {
        Intent i = new Intent();
        i.setClassName("com.example.secondapp", "com.example.secondapp.recievers.CameraVideo"); // (other app package name, other app reciever class)
        i.putExtra("CameraVideo", true); // The information that we want to send is sent by putExtra(key, value)

        sendBroadcast(i);
        System.out.println("CameraVideo success.");
    }

    @Override
    public void testFailure() {
        Intent i = new Intent();
        i.setClassName("com.example.secondapp", "com.example.secondapp.recievers.CameraVideo"); // (other app package name, other app reciever class)
        i.putExtra("CameraVideo", false); // The information that we want to send is sent by putExtra(key, value)

        sendBroadcast(i);
        System.out.println("CameraVideo failure.");
    }
}
