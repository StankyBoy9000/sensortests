package com.example.securazediagnose.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;

import com.example.securazediagnose.R;

public class VolumeControll extends AppCompatActivity implements KeyEvent.Callback{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volume_controll);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_DOWN)){
            System.out.println("Volume down");
        }
        else if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            System.out.println("Volume up");
        }
        return true;
    }

}
