package com.example.securazediagnose.adb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Command {

    private String deviceId;
    protected final String PACKAGE_NAME = "com.example.securazediagnose";

    public Command(){
    }

    public String runADBCommand(String adbCommand) throws IOException {
        StringBuffer returnValue = new StringBuffer();
        String line;
        InputStream inStream = null;
        try {
            System.out.println("adbCommand = " + adbCommand);
            Process process = Runtime.getRuntime().exec(adbCommand);

            // process.waitFor();/
            inStream = process.getInputStream();
            BufferedReader brCleanUp = new BufferedReader(new InputStreamReader(inStream));
            while ((line = brCleanUp.readLine()) != null) {
                line = line.replace(".","");
                String[] list = line.split("'");
                if(list.length >= 2){
                    line = line.split("'")[1];
                }
                returnValue.append(line);
            }
            brCleanUp.close();
            try {
                process.waitFor();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        }
        System.out.println(returnValue.toString() + "@@");
        return returnValue.toString();
    }


    public void setDeviceId(String deviceId){
        this.deviceId = deviceId;
    }

    public String getDeviceId(){

        return this.deviceId;
    }

    public String checkPermissions() throws IOException {
        return runADBCommand("dumpsys package " + PACKAGE_NAME);
    }
}