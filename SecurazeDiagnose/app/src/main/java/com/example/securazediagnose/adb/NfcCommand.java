package com.example.securazediagnose.adb;

import java.io.IOException;

public class NfcCommand extends Command {

    public NfcCommand() {
    }

    public void grantPermission() throws IOException {
        runADBCommand("pm grant " + PACKAGE_NAME + " android.permission.WRITE_SECURE_SETTINGS");
    }

    public void revokePermission() throws IOException {
        runADBCommand("pm revoke " + PACKAGE_NAME + " android.permission.WRITE_SECURE_SETTINGS");
    }

    public void activateNFC() throws IOException {
        runADBCommand("service call nfc 8");
    }

    public void deactivateNFC() throws IOException {
        runADBCommand("service call nfc 7");
    }
}
