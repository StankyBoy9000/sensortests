package com.example.securazediagnose.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.Spinner;

import com.example.securazediagnose.EventListener;
import com.example.securazediagnose.OnSwipeTouchListener;
import com.example.securazediagnose.R;
import com.example.securazediagnose.store.SensorList;
import com.example.securazediagnose.store.StateVO;

import java.util.ArrayList;


public class CheckboxView extends Fragment {
    Spinner sensorsSpinner;
    SensorList sensorList;
    CheckBox allSensorsCheckbox;
    Button start;

    private EventListener listener;
    View checkboxView;
    FrameLayout frameLayout;
    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        if(activity instanceof EventListener) {
            listener = (EventListener)activity;
        } else {
            // Throw an error!
        }
    }

    public CheckboxView() {
        // Required empty public constructor
    }


    public static CheckboxView newInstance(String param1, String param2) {
        CheckboxView fragment = new CheckboxView();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
//            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        checkboxView = inflater.inflate(R.layout.fragment_checkbox_view, container, false);
        sensorList = new SensorList();
        sensorsSpinner = (Spinner) checkboxView.findViewById(R.id.sensorSpinner);
        allSensorsCheckbox = checkboxView.findViewById(R.id.allSensorsCheckbox);
        allSensorsCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                  sensorList.checkAll(isChecked);
              }
          }
        );
        start = checkboxView.findViewById(R.id.startTests);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                sensorList.startTests(getActivity().getApplicationContext());
                getProgressView();
            }
        });
        SecurazeAdapter myAdapter = new SecurazeAdapter(getActivity(), 0, sensorList.getListVOs());
        sensorsSpinner.setAdapter(myAdapter);

        frameLayout = getActivity().findViewById(R.id.fragment_container);
        frameLayout.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {

            @Override
            public void onSwipeLeft() {
                super.onSwipeRight();
                getProgressView();
            }
        });
        return checkboxView;
    }

    public void getProgressView(){
        ArrayList<StateVO> testList = sensorList.getActiveTests();
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < testList.size(); i++){
            list.add(testList.get(i).getTitle());
            System.out.println(testList.get(i).getTitle());
        }
        ProgressView pv = new ProgressView();
        Bundle bundle = new Bundle();
        bundle.putStringArrayList("activeTests", list);
        pv.setArguments(bundle);
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, pv, "findThisFragment")
                .addToBackStack(null)
                .commit();
    }
}
