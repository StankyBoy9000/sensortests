package com.example.securazediagnose.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.securazediagnose.EventListener;
import com.example.securazediagnose.OnSwipeTouchListener;
import com.example.securazediagnose.R;
import com.example.securazediagnose.store.SensorList;

import java.util.ArrayList;


public class ProgressView extends Fragment {
    ArrayList<String> activeTestsList;
    private EventListener listener;
    View progressView;
    FrameLayout frameLayout;
    LinearLayout linLayout;
    SensorList sensorList;
    Button stop;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        if(activity instanceof EventListener) {
            listener = (EventListener)activity;
        } else {
            // Throw an error!
        }
    }

    public ProgressView() {
        // Required empty public constructor
    }



    public static ProgressView newInstance(String param1, String param2) {
        ProgressView fragment = new ProgressView();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IntentFilter intentFilter = new IntentFilter();
        Bundle args = getArguments();
        if (args != null) {

            activeTestsList = getArguments().getStringArrayList("activeTests");
            sensorList = new SensorList(activeTestsList);
            sensorList.startSelectedTests(getActivity().getApplicationContext());
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        progressView = inflater.inflate(R.layout.fragment_progress_view, container, false);
        linLayout = (LinearLayout) progressView.findViewById(R.id.testCaseProgressLayout);

        for(int i = 0; i < activeTestsList.size(); i++){
            View testCase = getLayoutInflater().inflate(R.layout.test_case_loadingbar, null);
            TextView title = testCase.findViewById(R.id.case_title);
            title.setText(activeTestsList.get(i));
            linLayout.addView(testCase);
        }
        stop = progressView.findViewById(R.id.stopTests);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sensorList.stopTests(getActivity().getApplicationContext());
            }
        });
        frameLayout = getActivity().findViewById(R.id.fragment_container);
        frameLayout.setOnTouchListener(new OnSwipeTouchListener(getActivity()) {

            public void onSwipeRight() {
                super.onSwipeRight();
                CheckboxView cv = new CheckboxView();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, cv, "findThisFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });
        return progressView;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter("TEST_RESULT"));
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String caseName = intent.getStringExtra("CaseName");
            System.out.println("Action: " + caseName);

            for(int i = 0; i < linLayout.getChildCount(); i++){
                TextView caseTitle = linLayout.getChildAt(i).findViewById(R.id.case_title);
                if(caseTitle.getText().equals(caseName)){
                    sensorList.removeTest(caseName);
                    boolean testResult = intent.getBooleanExtra("Result",false);
                    FrameLayout loadingFrame = linLayout.getChildAt(i).findViewById(R.id.statusIconContainer);
                    loadingFrame.removeAllViews();
                    ImageView imageView = new ImageView(getActivity());
                    if(testResult){
                        imageView.setImageResource(R.drawable.test_success);
                    }else{
                        imageView.setImageResource(R.drawable.test_failure);
                    }
                    loadingFrame.addView(imageView);
                }
            }
        }
    };

    public void startCheckedTests(){

    }


}
