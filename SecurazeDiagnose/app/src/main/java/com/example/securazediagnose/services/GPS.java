package com.example.securazediagnose.services;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class GPS extends Service {
    LocationManager mLocationManager;
    private LocationListener locationListener;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;
    double longitude = 0;
    double latitude = 0;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("GPS service started.");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("Permissions not granted");
            testFailure();
        }
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }

        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            System.out.println("fail to request location update, ignore "+ ex);
            testFailure();
        } catch (IllegalArgumentException ex) {
            System.out.println( "network provider does not exist, " + ex.getMessage());
            testFailure();
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            System.out.println( "fail to request location update, ignore "+ ex);
            testFailure();
        } catch (IllegalArgumentException ex) {
            System.out.println( "gps provider does not exist " + ex.getMessage());
            testFailure();
        }
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    System.out.println("fail to remove location listners, ignore: " + ex);
                }
            }
        }
        System.out.println("GPS service destroyed.");
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public String getLocation(Location loc) {
        longitude = loc.getLongitude();
        latitude = loc.getLatitude();
        String mLongitude = "Longitude: " + longitude;
        String mLatitude = "Latitude: " + latitude;
        /*------- To get city name from coordinates -------- */
        Geocoder gcd = new Geocoder(getBaseContext(), Locale.getDefault());

        String s = mLongitude + "\n" + mLatitude + "\n";
        System.out.println(s);
        return s;
    }

    private void testSuccess(Location loc){
        String location = getLocation(loc);
        Intent i = new Intent("GPS_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("GPSCase", true); // The information that we want to send is sent by putExtra(key, value)
        i.putExtra("Longitude", longitude);
        i.putExtra("Latitude", latitude);

        sendBroadcast(i);
        System.out.println("GPS success.");
        stopSelf();
    }

    private void testFailure(){
        Intent i = new Intent("GPS_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("GPSCase", false); // The information that we want to send is sent by putExtra(key, value)

        sendBroadcast(i);
        System.out.println("GPS failed.");
        stopSelf();
    }


    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            System.out.println("LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            System.out.println("onLocationChanged: " + location);
            mLastLocation.set(location);
            testSuccess(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            System.out.println( "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            System.out.println( "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            System.out.println("onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
}
