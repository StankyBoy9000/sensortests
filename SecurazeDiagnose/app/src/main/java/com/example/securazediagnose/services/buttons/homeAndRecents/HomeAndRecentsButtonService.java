package com.example.securazediagnose.services.buttons.homeAndRecents;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.example.securazediagnose.Test;

public class HomeAndRecentsButtonService extends Service implements Test {
    private HomeWatcher mHomeWatcher;
    private boolean homeButtonPressed = false;
    private boolean recentsButtonPressed = false;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("HomeAndRecetntsButton service activated.");
        mHomeWatcher = new HomeWatcher(this);
        mHomeWatcher.setOnHomePressedListener(new OnHomePressedListener() {
            @Override
            public void onHomePressed() {
                System.out.println("Home pressed.");
                homeButtonPressed = true;
                if(homeButtonPressed && recentsButtonPressed){
                    testSuccess();
                }
            }
            @Override
            public void onHomeLongPressed() {
                System.out.println("Recent apps pressed");
                recentsButtonPressed = true;
                if(homeButtonPressed && recentsButtonPressed){
                    testSuccess();
                }

            }
        });
        mHomeWatcher.startWatch();
        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void onDestroy() {
        System.out.println("HomeAndRecetntsButton service destroyed.");
        mHomeWatcher.stopWatch();
    }
    public void testSuccess(){
        Intent i = new Intent();
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.HomeAndRecetntsButtonReceiver"); // (other app package name, other app reciever class)
        i.putExtra("HomeAndRecetntsButtonCase", true); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        stopSelf();
    }
    public void testFailure(){
        Intent i = new Intent();
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.HomeAndRecetntsButtonReceiver"); // (other app package name, other app reciever class)
        i.putExtra("HomeAndRecetntsButtonCase", false); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        stopSelf();
    }
}
