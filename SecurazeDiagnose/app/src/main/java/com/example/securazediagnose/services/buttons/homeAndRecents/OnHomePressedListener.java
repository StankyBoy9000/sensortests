package com.example.securazediagnose.services.buttons.homeAndRecents;

public interface OnHomePressedListener {
    void onHomePressed();
    void onHomeLongPressed();
}
