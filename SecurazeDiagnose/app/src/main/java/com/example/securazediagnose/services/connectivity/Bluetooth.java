package com.example.securazediagnose.services.connectivity;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.IBinder;



import androidx.annotation.Nullable;

import com.example.securazediagnose.Test;

import java.util.Set;
import java.util.concurrent.TimeUnit;

public class Bluetooth extends Service implements Test {

    private BluetoothAdapter bAdapter;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    private Set<BluetoothDevice> getAllDevices(){
        Set<BluetoothDevice> pairedDevices = bAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                System.out.println("Device name: " + deviceName + "\nDevice Hardware adress: " + deviceHardwareAddress);
            }
        }
        return pairedDevices;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Bluetooth service activated.");

        bAdapter = BluetoothAdapter.getDefaultAdapter();
        try {
            testBluetooth();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        System.out.println("Bluetooth service destroyed.");
    }

    private boolean activateBluetooth(){
        if(bAdapter == null){
            System.out.println("Bluetooth not supported.");
            return false;
        }
        if(!bAdapter.isEnabled()){
            BluetoothAdapter.getDefaultAdapter().enable();
            System.out.println("Bluetooth Turned ON.");
        }
        return true;
    }

    private boolean deactivateBluetooth(){
        if(bAdapter.isEnabled()){
            BluetoothAdapter.getDefaultAdapter().disable();
            return true;
        }
        return false;
    }

    private boolean testBluetooth() throws InterruptedException {
        boolean activation = activateBluetooth();

        TimeUnit.SECONDS.sleep(1);
        boolean deactivation = deactivateBluetooth();
        if(activation && deactivation){
            testSuccess();
        }else{
            testFailure();
        }
        return activation && deactivation;
    }


    @Override
    public void testSuccess() {
        Intent i = new Intent("BLUETOOTH_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("BluetoothCase", true); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("Bluetooth success.");
        stopSelf();
    }

    @Override
    public void testFailure() {
        Intent i = new Intent("BLUETOOTH_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("BluetoothCase", false); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("Bluetooth failure.");
        stopSelf();
    }
}
