package com.example.securazediagnose.services.connectivity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

import androidx.annotation.Nullable;

import com.example.securazediagnose.Test;

import java.util.concurrent.TimeUnit;

public class SimAndSignal extends Service implements Test {
    MyPhoneStateListener mPhoneStatelistener;
    int mSignalStrength = 0;
    TelephonyManager telMgr;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Sim and Signal service activated.");
        telMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
                System.out.println("SIM STATUS: Sim absent.");
                testFailure();
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                System.out.println("SIM STATUS: Network locked.");
                testFailure();
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                System.out.println("SIM STATUS: Sim pin required. ");
                testFailure();
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                System.out.println("SIM STATUS: Sim puk required.");
                testSuccess();
                break;
            case TelephonyManager.SIM_STATE_READY:
                System.out.println("SIM STATUS: Sim ready.");
                mPhoneStatelistener = new MyPhoneStateListener();
                telMgr.listen(mPhoneStatelistener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);


                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
                // do something
                break;
        }

        return super.onStartCommand(intent, flags, startId);
    }
    @Override
    public void testFailure(){
        Intent i = new Intent("SIM_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("SimCase", false); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("Sim failure.");
        stopSelf();
    }
    @Override
    public void testSuccess(){
        Intent i = new Intent("SIM_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("SimCase", true); // The information that we want to send is sent by putExtra(key, value)
        i.putExtra("SignalStrength", mSignalStrength);

        sendBroadcast(i);
        System.out.println("Sim success.");
        stopSelf();
    }
    @Override
    public void onDestroy() {

        System.out.println("Sim and Signal service destroyed.");
    }

    class MyPhoneStateListener extends PhoneStateListener {

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength) {
            super.onSignalStrengthsChanged(signalStrength);
            mSignalStrength = signalStrength.getGsmSignalStrength();
            System.out.println("Signal strength: "+mSignalStrength);
            if(mSignalStrength > 0){
                testSuccess();
            }
            else{
                testFailure();
            }
        }
    }
}
