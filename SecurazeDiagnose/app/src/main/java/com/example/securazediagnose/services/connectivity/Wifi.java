package com.example.securazediagnose.services.connectivity;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.example.securazediagnose.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class Wifi extends Service implements Test {
    // NOTE: Doesn't work on android 10
    private WifiManager wifi;
    List<ScanResult> mScanResults;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        try {
            testWifi();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Wifi service activated.");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        System.out.println("Wifi service destroyed.");
    }

    private List<ScanResult> getAvailableNetworks(){
        List<ScanResult> mScanResults = wifi.getScanResults();
        if(mScanResults.size() > 0){
            for (int i = 0; i < mScanResults.size(); i++){
                System.out.println(mScanResults.get(i).toString());
            }
        }else{
            System.out.println("No networks found.");
        } return mScanResults;
    }
    @Override
    public void testSuccess() {
        Intent i = new Intent("WIFI_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("WifiCase", true); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("Wifi success.");
        stopSelf();
    }

    @Override
    public void testFailure() {
        Intent i = new Intent("WIFI_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("WifiCase", false); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("Wifi failure.");
        stopSelf();
    }

    private void activateWifi(){
        if (!wifi.isWifiEnabled()) {
            wifi.setWifiEnabled(true);
        }
    }

    private void deactivateWifi(){
        if(wifi.isWifiEnabled()){
            wifi.setWifiEnabled(false);
        }
    }

    private void testWifi() throws InterruptedException {
        activateWifi();
        TimeUnit.SECONDS.sleep(2);
        boolean activation = wifi.isWifiEnabled();
        deactivateWifi();
        TimeUnit.SECONDS.sleep(1);
        boolean deactivation = !wifi.isWifiEnabled();
        if (activation && deactivation) {
            testSuccess();
        } else {
            testFailure();
        }
    }


}