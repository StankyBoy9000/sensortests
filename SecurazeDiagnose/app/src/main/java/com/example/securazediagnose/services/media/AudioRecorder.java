package com.example.securazediagnose.services.media;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.IBinder;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.example.securazediagnose.Test;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class AudioRecorder extends Service implements Test {
    //NOTE: Need buttons to start recording and passed failed buttons that will stop the service

    final MediaRecorder recorder = new MediaRecorder();
    public String path;


    public AudioRecorder() {
    }

    private String sanitizePath() {
        File new_folder = new File(getExternalFilesDir(null), "/AudioRecorder");
        if(!new_folder.exists()){
            new_folder.mkdir();
        }
        File input_file = new File(new_folder, "sound.3gp");

        return input_file.toString();
    }

    public void start() throws IOException {
        String state = android.os.Environment.getExternalStorageState();
        if (!state.equals(android.os.Environment.MEDIA_MOUNTED)) {
            throw new IOException("SD Card is not mounted.  It is " + state
                    + ".");
        }

        // make sure the directory we plan to store the recording in exists
        File directory = new File(path).getParentFile();
        if (!directory.exists() && !directory.mkdirs()) {
            throw new IOException("Path to file could not be created.");
        }

        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        recorder.setOutputFile(path);
        recorder.prepare();
        recorder.start();
    }

    public void stop() throws IOException {
        recorder.stop();
        recorder.release();
    }

    public void playarcoding(String path) throws IOException {
        Toast.makeText(getApplicationContext(),"PLAYING SOUND", Toast.LENGTH_SHORT).show();
        MediaPlayer mp = new MediaPlayer();
        mp.setDataSource(path);
        mp.prepare();
        mp.start();
        mp.setVolume(10, 10);
    }
    @Override
    public void testFailure(){
        Intent i = new Intent("SOUND_AND_MIC_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("AudioRecorderCase", false); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("AudioRecorder fail.");
        stopSelf();
    }
    @Override
    public void testSuccess(){
        Intent i = new Intent("SOUND_AND_MIC_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("AudioRecorderCase", true); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("AudioRecorder success.");
        stopSelf();
    }

    @Override
    public void onCreate() {
        this.path = sanitizePath();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("AudioRecorder service created");
        try {
            Toast.makeText(getApplicationContext(),"RECORDING", Toast.LENGTH_SHORT).show();
            start();
            TimeUnit.SECONDS.sleep(3);
            stop();
            playarcoding(path);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        System.out.println("AudioRecorder service destroyed");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
