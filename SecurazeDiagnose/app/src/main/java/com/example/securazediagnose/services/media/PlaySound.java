package com.example.securazediagnose.services.media;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import com.example.securazediagnose.R;
import com.example.securazediagnose.Test;

import java.io.File;

public class PlaySound extends Service implements Test {
    //NOTE: Add buttons for testSuccess and testFail to stop the service!

    private MediaPlayer player;
    private String path = "";
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        if(path.equals("")){
            player = MediaPlayer.create(this, R.raw.sunsetlover); // R.raw.sunsetlover (selected music file) NOTE: the song name needs to be lowercase! If it's not, the whole raw folder will not be visible
            player.setLooping(false); //set looping
        }
        else{
            try {
                player.setDataSource(path);
                player.prepare();
                player.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        player.start();
        return Service.START_NOT_STICKY;
    }

    public void onDestroy() {
        player.stop();
        player.release();
        stopSelf();
        super.onDestroy();
    }
    @Override
    public void testFailure(){
        Intent i = new Intent("SOUND_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("SoundCase", false); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("Sound fail.");
        stopSelf();
    }
    @Override
    public void testSuccess(){
        Intent i = new Intent("SOUND_CASE");
        i.setClassName("com.example.secondapp", "com.example.secondapp.receivers.GeneralReceiver"); // (other app package name, other app reciever class)
        i.putExtra("SoundCase", true); // The information that we want to send is sent by putExtra(key, value)
        sendBroadcast(i);
        System.out.println("Sound success.");
        stopSelf();
    }

    public void setAudioPath(String audioPath){
        path = audioPath;
    }

    public String getAudioPath(){
        return path;
    }
}
