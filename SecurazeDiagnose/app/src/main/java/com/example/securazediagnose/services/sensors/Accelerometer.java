package com.example.securazediagnose.services.sensors;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import java.lang.Math;

import androidx.annotation.Nullable;

public class Accelerometer extends SensorService implements SensorEventListener, TestSensorEvent {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {}

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(sensorManager == null){
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        }
        if(sensor == null){
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        }
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
        System.out.println("Accelometer service activated.");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {
        sensorManager.unregisterListener(this, sensor);
        testFailure();
        System.out.println("Accelometer service destroyed.");
    }

    @Override
    public void onSensorChanged(SensorEvent event){
        if(sensorCounter < 3){
            initialX = event.values[0];
            initialY = event.values[1];
            initialZ= event.values[2];
        }
        else{
            if(roundAvoid(initialX,1) != roundAvoid(event.values[0],1) || roundAvoid(initialY,1) != roundAvoid(event.values[1],1) || roundAvoid(initialZ,1) != roundAvoid(event.values[2],1)){
                testSuccess(event);
            }
        }
//        System.out.println("Accelometer first time: X:" + roundAvoid(event.values[0],2) + " Y: " + event.values[1] + " Z: " + event.values[2]);
//        System.out.println("Accelometer sensor working...");
        sensorCounter++;
    }

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//        System.out.println("Accelometer accuracy changed");
    }

    @Override
    public void testSuccess(SensorEvent event) {
        if(!testFlag){
        Intent i = new Intent("TEST_RESULT");
        i.putExtra("CaseName", "Accelerometer");
        i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
        i.putExtra("AccelerometerX", event.values[0]);
        i.putExtra("AccelerometerY", event.values[1]);
        i.putExtra("AccelerometerZ", event.values[2]); // When data is recieved, you do not have to extract all of it. This can be used when needed.
        sendBroadcast(i);
        System.out.println("Accelometer success.");
        stopSelf();
        }
        testFlag = true;
    }

    @Override
    public void testFailure(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Accelerometer");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("AccelerometerX", event.values[0]);
            i.putExtra("AccelerometerY", event.values[1]);
            i.putExtra("AccelerometerZ", event.values[2]); // When data is recieved, you do not have to extract all of it. This can be used when needed.
            sendBroadcast(i);
            System.out.println("Accelometer failure.");
            stopSelf();
        }
        testFlag = true;
    }

    public void testFailure() {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Accelerometer");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            sendBroadcast(i);
            System.out.println("Accelometer failure.");
            stopSelf();
        }
        testFlag = true;
    }


}
