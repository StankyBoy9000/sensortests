package com.example.securazediagnose.services.sensors;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.securazediagnose.Test;

public class Fingerprint extends SensorService implements Test {
    FingerprintManager fingerprintManager;
    String message = "";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Fingerprint service activated.");
        fingerprintManager = (FingerprintManager) getApplicationContext().getSystemService(Context.FINGERPRINT_SERVICE);
        if (!fingerprintManager.isHardwareDetected()) {
            message = "Device doesn't support fingerprint authentication";
            System.out.println("Device doesn't support fingerprint authentication");
            testFailure();
        } else if (!fingerprintManager.hasEnrolledFingerprints()) {
            message = "User hasn't enrolled any fingerprints to authenticate with";
            System.out.println("User hasn't enrolled any fingerprints to authenticate with");
            testSuccess();
        } else {
            message = "Everything is ready for fingerprint authentication";
            System.out.println("Everything is ready for fingerprint authentication");
            testSuccess();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {
        testFailure();
        System.out.println("Fingerprint service destroyed.");
    }

    @Override
    public void testFailure(){
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Fingerprint");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("Message", message);
            sendBroadcast(i);
            System.out.println("Fingerprint fail.");
            stopSelf();
        }
        testFlag = true;
    }

    @Override
    public void testSuccess(){
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Fingerprint");
            i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("Message", message);
            sendBroadcast(i);
            System.out.println("Fingerprint success.");
            stopSelf();
        }
        testFlag = true;
    }
}
