package com.example.securazediagnose.services.sensors;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class Gravity extends SensorService implements SensorEventListener, TestSensorEvent {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(sensorManager == null){
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        }
        if(sensor == null){
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        }
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        System.out.println("Gravity service activated.");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {
        sensorManager.unregisterListener(this, sensor);
        System.out.println("Gravity service destroyed.");
        testFailure();
    }
    @Override
    public void onSensorChanged(SensorEvent event){
        if(sensorCounter < 3){
            initialX = event.values[0];
            initialY = event.values[1];
            initialZ= event.values[2];
        }
        else{
            if(roundAvoid(initialX,1) != roundAvoid(event.values[0],1) || roundAvoid(initialY,1) != roundAvoid(event.values[1],1) || roundAvoid(initialZ,1) != roundAvoid(event.values[2],1)){
                testSuccess(event);
            }
        }
//        System.out.println("Gravity: X:" + roundAvoid(event.values[0],1) + " Y: " + roundAvoid(event.values[1],1) + " Z: " + roundAvoid(event.values[2],1));
//        System.out.println("Gravity sensor working...");
        sensorCounter++;

    }
    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//        System.out.println("Gravity accuracy changed");
    }


    @Override
    public void testSuccess(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Gravity");
            i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("GravityX", event.values[0]);
            i.putExtra("GravityY", event.values[1]);
            i.putExtra("GravityZ", event.values[2]); // When data is recieved, you do not have to extract all of it. This can be used when needed.
            sendBroadcast(i);
            System.out.println("Gravity success.");
            stopSelf();
        }
        testFlag = true;
    }

    @Override
    public void testFailure(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Gravity");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("GravityX", event.values[0]);
            i.putExtra("GravityY", event.values[1]);
            i.putExtra("GravityZ", event.values[2]); // When data is received, you do not have to extract all of it. This can be used when needed.
            sendBroadcast(i);
            System.out.println("Gravity failure.");
            stopSelf();
        }
        testFlag = true;
    }

    public void testFailure() {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Gravity");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            sendBroadcast(i);
            System.out.println("Gravity failure.");
            stopSelf();
        }
        testFlag = true;
    }
}
