package com.example.securazediagnose.services.sensors;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import java.lang.Math;
import android.util.Half;

import androidx.annotation.Nullable;

public class Gyroscope extends SensorService implements SensorEventListener, TestSensorEvent {


    private static final float NS2S = 1.0f / 1000000000.0f;
    private final float[] deltaRotationVector = new float[4];
    private float timestamp;
    private int numberOfCalculations;
    private float oldCalc;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        numberOfCalculations = 0;
        if(sensorManager == null){
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        }
        if(sensor == null){
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        }
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        System.out.println("Gyroscope service activated.");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {
        sensorManager.unregisterListener(this, sensor);
        System.out.println("Gyroscope service destroyed.");
        testFailure();
    }
    @Override
    public void onSensorChanged(SensorEvent event){
//        System.out.println("Gyroscope sensor changed");
//        System.out.println("Gyroscope: X:" + event.values[0] + " Y: " + event.values[1] + " Z: " + event.values[2]);
        if(numberOfCalculations > 3){
            if(roundAvoid(oldCalc,1) != roundAvoid(calculateRotation(event),1)){
                testSuccess(event);
            }
        }
        oldCalc = calculateRotation(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//        System.out.println("Gyroscope accuracy changed");
    }

    private float calculateRotation(SensorEvent event){
        // This timestep's delta rotation to be multiplied by the current rotation
        // after computing it from the gyro sample data.
        if (timestamp != 0) {
            final float dT = (event.timestamp - timestamp) * NS2S;
            // Axis of the rotation sample, not normalized yet.
            float axisX = (float) roundAvoid(event.values[0],1);
            float axisY = (float) roundAvoid(event.values[1],1);
            float axisZ = (float) roundAvoid(event.values[2],1);

            // Calculate the angular speed of the sample
            float omegaMagnitude = (float) Math.sqrt(axisX*axisX + axisY*axisY + axisZ*axisZ);

            // Normalize the rotation vector if it's big enough to get the axis
            // (that is, EPSILON should represent your maximum allowable margin of error)
            if (omegaMagnitude > Half.EPSILON) {
                axisX /= omegaMagnitude;
                axisY /= omegaMagnitude;
                axisZ /= omegaMagnitude;
            }

            // Integrate around this axis with the angular speed by the timestep
            // in order to get a delta rotation from this sample over the timestep
            // We will convert this axis-angle representation of the delta rotation
            // into a quaternion before turning it into the rotation matrix.
            float thetaOverTwo = omegaMagnitude * dT / 2.0f;
            float sinThetaOverTwo = (float) Math.sin(thetaOverTwo);
            float cosThetaOverTwo = (float) Math.cos(thetaOverTwo);
            deltaRotationVector[0] = sinThetaOverTwo * axisX;
            deltaRotationVector[1] = sinThetaOverTwo * axisY;
            deltaRotationVector[2] = sinThetaOverTwo * axisZ;
            deltaRotationVector[3] = cosThetaOverTwo;
        }
        timestamp = event.timestamp;
        float[] deltaRotationMatrix = new float[9];
        SensorManager.getRotationMatrixFromVector(deltaRotationMatrix, deltaRotationVector);
        float temp = 0;
        for(int i = 0; i < deltaRotationMatrix.length ; i++){
            temp += deltaRotationMatrix[i];
        }
        numberOfCalculations++;
        return temp;
        // User code should concatenate the delta rotation we computed with the current rotation
        // in order to get the updated rotation.
        // rotationCurrent = rotationCurrent * deltaRotationMatrix;
    }

    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }

    @Override
    public void testSuccess(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Gyroscope");
            i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
            sendBroadcast(i);
            System.out.println("Gyroscope success.");
            stopSelf();
        }
        testFlag = true;
    }

    @Override
    public void testFailure(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Gyroscope");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            sendBroadcast(i);
            System.out.println("Gyroscope failure.");
            stopSelf();
        }
        testFlag = true;
    }

    public void testFailure() {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Gyroscope");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            sendBroadcast(i);
            System.out.println("Gyroscope failure.");
            stopSelf();
        }
        testFlag = true;
    }
}
