package com.example.securazediagnose.services.sensors;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class Proximity extends SensorService implements SensorEventListener, TestSensorEvent {

    private float initialDistance;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(sensorManager == null){
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        }
        if(sensor == null){
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        }
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        System.out.println("Proximity service activated.");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        sensorManager.unregisterListener(this, sensor);
        System.out.println("Proximity service destroyed.");
        testFailure();
    }
    @Override
    public void onSensorChanged(SensorEvent event){
        if(sensorCounter < 1){
            initialDistance = event.values[0];
        }
        else{
            if(roundAvoid(initialDistance,1) != roundAvoid(event.values[0],1)){
                testSuccess(event);
            }
        }
//        System.out.println("Proximity: X:" + roundAvoid(event.values[0],1));
//        System.out.println("Proximity sensor working...");
        sensorCounter++;

    }
    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//        System.out.println("Proximity accuracy changed");
    }

    @Override
    public void testSuccess(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Proximity");
            i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("ProximityX", event.values[0]); // When data is received, you do not have to extract all of it. This can be used when needed.
            sendBroadcast(i);
            System.out.println("Proximity success.");
            stopSelf();
        }
        testFlag = true;
    }

    @Override
    public void testFailure(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Proximity");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("ProximityX", event.values[0]); // When data is recieved, you do not have to extract all of it. This can be used when needed.
            sendBroadcast(i);
            System.out.println("Proximity failure.");
            stopSelf();
        }
        testFlag = true;
    }

    public void testFailure() {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Proximity");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            sendBroadcast(i);
            System.out.println("Proximity failure.");
            stopSelf();
        }
        testFlag = true;
    }
}
