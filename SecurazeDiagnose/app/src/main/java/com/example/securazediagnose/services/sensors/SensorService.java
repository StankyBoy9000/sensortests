package com.example.securazediagnose.services.sensors;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.IBinder;


public abstract class SensorService extends Service {
    protected float initialX;
    protected float initialY;
    protected float initialZ;
    protected SensorManager sensorManager;
    protected Sensor sensor;
    protected boolean testFlag = false;
    protected int sensorCounter = 0;


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
