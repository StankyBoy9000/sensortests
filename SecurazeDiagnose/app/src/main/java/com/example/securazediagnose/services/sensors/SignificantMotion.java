package com.example.securazediagnose.services.sensors;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.example.securazediagnose.Test;

public class SignificantMotion extends SensorService implements Test {
    // A significant motion is a motion that might lead to a change in the user's location;
    // for example walking, biking, or sitting in a moving car.

    // NOT TESTED

    private TriggerEventListener triggerEventListener;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(sensorManager == null){
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        }
        if(sensor == null){
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
        }
        triggerEventListener = new TriggerEventListener() {
            @Override
            public void onTrigger(TriggerEvent event) {
                testSuccess();
            }
        };

        sensorManager.requestTriggerSensor(triggerEventListener, sensor);

        System.out.println("Significant motion service activated.");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        System.out.println("Significant motion service destroyed.");
        testFailure();
    }

    @Override
    public void testSuccess() {
        Intent i = new Intent("TEST_RESULT");
        i.putExtra("CaseName", "SignificantMotion");
        i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
        System.out.println("Significant Motion success");
        sendBroadcast(i);
        stopSelf();
    }

    @Override
    public void testFailure() {
        Intent i = new Intent("TEST_RESULT");
        i.putExtra("CaseName", "SignificantMotion");
        i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
        System.out.println("Significant Motion failure");
        sendBroadcast(i);
        stopSelf();
    }
}
