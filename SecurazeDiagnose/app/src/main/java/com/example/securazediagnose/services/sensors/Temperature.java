package com.example.securazediagnose.services.sensors;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.example.securazediagnose.Test;

public class Temperature extends SensorService implements SensorEventListener, TestSensorEvent, Test {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(sensorManager == null){
            sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        }
        if(sensor == null){
            sensor = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
            if(sensor == null){
                testFailure();
            }
        }

        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);

        System.out.println("Temperature service activated.");

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy()
    {
        sensorManager.unregisterListener(this, sensor);
        System.out.println("Temperature service destroyed.");
        testFailure();
    }
    @Override
    public void onSensorChanged(SensorEvent event){
            initialX = event.values[0];
            if(initialX == 0.0f){
                testFailure(event);
            }
            else{
                testSuccess(event);
            }

        System.out.println("Temperature: X:" + roundAvoid(event.values[0],1));
//        System.out.println("Compass sensor working...");

    }
    public static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
//        System.out.println("Compass accuracy changed");
    }

    @Override
    public void testFailure(SensorEvent event){
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Temperature");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            sendBroadcast(i);
            System.out.println("Temperature failure.");
            stopSelf();
        }
        testFlag = true;
    }

    @Override
    public void testSuccess(SensorEvent event) {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Temperature");
            i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("Degrees", event.values[0]); // When data is recieved, you do not have to extract all of it. This can be used when needed.
            sendBroadcast(i);
            System.out.println("Temperature success.");
            stopSelf();
        }
        testFlag = true;
    }


    @Override
    public void testSuccess() {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("Result", true); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("CaseName", "Temperature");
            sendBroadcast(i);
            System.out.println("Temperature success.");
            stopSelf();
        }
        testFlag = true;
    }

    @Override
    public void testFailure() {
        if(!testFlag) {
            Intent i = new Intent("TEST_RESULT");
            i.putExtra("CaseName", "Temperature");
            i.putExtra("Result", false); // The information that we want to send is sent by putExtra(key, value)
            i.putExtra("Temperature", initialX);
            sendBroadcast(i);
            System.out.println("Temperature failure.");
            stopSelf();
        }
        testFlag = true;
    }
}
