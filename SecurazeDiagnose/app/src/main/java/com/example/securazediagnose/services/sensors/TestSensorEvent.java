package com.example.securazediagnose.services.sensors;

import android.hardware.SensorEvent;

public interface TestSensorEvent {

    public void testSuccess(SensorEvent event);
    public void testFailure(SensorEvent event);
}
