package com.example.securazediagnose.store;

import android.content.Context;

import java.util.ArrayList;

public abstract class BaseList extends ArrayList<StateVO>{
    protected ArrayList<StateVO> listVOs;
    protected String[] sensors;


    public BaseList(){

    }
    public BaseList(String[] s){
        sensors = s;
        listVOs = new ArrayList<>();

        for (int i = 0; i < sensors.length; i++) {
            StateVO stateVO = new StateVO();
            stateVO.setTitle(sensors[i]);
            stateVO.setSelected(false);
            listVOs.add(stateVO);
        }
    }

    public ArrayList<StateVO> getActiveTests(){
        ArrayList<StateVO> activeTests = new ArrayList<StateVO>();
        for (int i = 0; i < listVOs.size(); i++){
            if(listVOs.get(i).isSelected() == true){
                activeTests.add(listVOs.get(i));
            }
        }
        return activeTests;
    }

    public void checkAll(boolean state){
        for (int i = 1; i < listVOs.size(); i++){
            listVOs.get(i).setSelected(state);
        }
    }

    public void startTests(Context context, ArrayList<String> list){
        for(int i = 0; i < list.size(); i++){
                startTest(list.get(i),context);
        }
    }

    public void startSelectedTests(Context context){
        for(int i = 0; i < listVOs.size(); i++){
            startTest(listVOs.get(i).getTitle(),context);
        }
    }

    public void stopTests(Context context){
        for(int i = 0; i < listVOs.size(); i++){
                stopTest(listVOs.get(i).getTitle(),context);
        }
    }

    public void removeTest(String name){
        for(int i = 0; i < listVOs.size(); i++){
            if(listVOs.get(i).getTitle().equals(name)){
                listVOs.remove(i);
            }
        }
    }

    abstract void startTest(String testName, Context context);
    abstract void stopTest(String testName, Context context);

    public String[] getSensors() {
        return sensors;
    }

    public void setSensors(String[] sensors) {
        this.sensors = sensors;
    }

    public ArrayList<StateVO> getListVOs() {
        return listVOs;
    }

    public void setListVOs(ArrayList<StateVO> listVOs) {
        this.listVOs = listVOs;
    }

}
