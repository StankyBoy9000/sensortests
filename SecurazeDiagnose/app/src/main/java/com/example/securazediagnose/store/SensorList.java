package com.example.securazediagnose.store;

import android.content.Context;
import android.content.Intent;

import com.example.securazediagnose.services.sensors.Accelerometer;
import com.example.securazediagnose.services.sensors.Barometer;
import com.example.securazediagnose.services.sensors.Compass;
import com.example.securazediagnose.services.sensors.Fingerprint;
import com.example.securazediagnose.services.sensors.Gravity;
import com.example.securazediagnose.services.sensors.Gyroscope;
import com.example.securazediagnose.services.sensors.Proximity;
import com.example.securazediagnose.services.sensors.Temperature;

import java.util.ArrayList;

public class SensorList extends BaseList{

    public SensorList(){
         this.sensors = new String[]{"Selected sensors", "Accelerometer", "Barometer", "Compass", "Fingerprint", "Gravity",
         "Gyroscope", "Proximity", "SignificantMotion", "Temperature"};

        this.listVOs = new ArrayList<>();

        for (int i = 0; i < sensors.length; i++) {
            StateVO stateVO = new StateVO();
            stateVO.setTitle(sensors[i]);
            stateVO.setSelected(false);
            listVOs.add(stateVO);
        }
    }

    public SensorList(ArrayList<String> list){
        this.listVOs = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            StateVO stateVO = new StateVO();
            stateVO.setTitle(list.get(i));
            stateVO.setSelected(false);
            listVOs.add(stateVO);
        }
    }

    @Override
    void startTest(String testName, Context context) {
        switch (testName){
            case "Accelerometer":
                context.startService(new Intent(context, Accelerometer.class));
                break;
            case "Barometer":
                context.startService(new Intent(context, Barometer.class));
                break;
            case "Compass":
                context.startService(new Intent(context, Compass.class));
                break;
            case "Fingerprint":
                context.startService(new Intent(context, Fingerprint.class));
                break;
            case "Gravity":
                context.startService(new Intent(context, Gravity.class));
                break;
            case "Gyroscope":
                context.startService(new Intent(context, Gyroscope.class));
                break;
            case "Proximity":
                context.startService(new Intent(context, Proximity.class));
                break;
            case "SignificantMotion":
//                context.startService(new Intent(context, SignificantMotion.class));
                break;
            case "Temperature":
                context.startService(new Intent(context, Temperature.class));
                break;
        }
    }

    @Override
    void stopTest(String testName, Context context) {
        switch (testName){
            case "Accelerometer":
                context.stopService(new Intent(context, Accelerometer.class));
                break;
            case "Barometer":
                context.stopService(new Intent(context, Barometer.class));
                break;
            case "Compass":
                context.stopService(new Intent(context, Compass.class));
                break;
            case "Fingerprint":
                context.stopService(new Intent(context, Fingerprint.class));
                break;
            case "Gravity":
                context.stopService(new Intent(context, Gravity.class));
                break;
            case "Gyroscope":
                context.stopService(new Intent(context, Gyroscope.class));
                break;
            case "Proximity":
                context.stopService(new Intent(context, Proximity.class));
                break;
            case "SignificantMotion":
//                context.stopService(new Intent(context, SignificantMotion.class));
                break;
            case "Temperature":
                context.stopService(new Intent(context, Temperature.class));
                break;
        }
    }


}
